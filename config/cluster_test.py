'''
    Example python script to be run like

        xAH_run.py --files ... --config gamma_b.py ... <driver>

    Note that unlike the JSON configuration example, you also have access to
the command line arguments passed in and parsed by xAH_run.py under the global
variable `args`. For example, there is an `isMC` flag which can be used to
configure your job options dynamically

        if not args.is_MC:
            systName = "Nominal"
            systVal = 0
        else:
            systName = "All"
            systVal = 1

    If you want to know what arguments are passed in, you can simply write

        print(args)

    to see the namespace associated with it, or look at the source xAH_run.py
here: https://github.com/UCATLAS/xAODAnaHelpers/blob/master/scripts/xAH_run.py
'''

from xAH_config import xAH_config
c = xAH_config()

c.setalg("BasicEventSelection", {"m_debug": False,
                                 "m_truthLevelOnly": False,
                                 "m_applyGRLCut": False,
                                 #"m_GRLxml": "config/data15_13TeV.periodAllYear_DetStatus-v73-pro19-08_DQDefects-00-01-02_PHYS_StandardGRL_All_Good_25ns.xml",
                                 "m_doPUreweighting": False,
                                 "m_vertexContainerName": "PrimaryVertices",
                                 "m_PVNTrack": 2,
                                 "m_useMetaData": False,
                                 "m_derivationName": "JETM8Kernel",
                                 "m_triggerSelection": "HLT_j360",
                                 "m_storeTrigDecisions": True,
                                 "m_applyTriggerCut": True,
                                 "m_storeTrigKeys": True,
                                 "m_storePassHLT": True
                                })

#c.setalg("ClusterHistsAlgo", { "m_detailStr": "",
#                               "m_name": "ClusterHistograms",
#                               "m_inContainerName": "CaloCalTopoClusters",
#                               "m_detailStr": "eta"
#                         })

c.setalg("JetCalibrator", {"m_name": "JetCalibration",
                           "m_debug": False,
                           "m_inContainerName": "AntiKt4EMTopoJets",
                           "m_outContainerName": "AntiKt4EMTopoJetsCalib",
                           "m_jetAlgo": "AntiKt4EMTopo"
                          })

c.setalg("JetHistsAlgo", {"m_debug": False,
                          "m_inContainerName": "AntiKt4EMTopoJetsCalib",
                          "m_detailStr": "kinematic clean energy layer trackPV",
                          "m_name": "NoPreSel"
                        })

c.setalg("JetSelector", {"m_name": "Selector450",
                         "m_inContainerName": "AntiKt4EMTopoJetsCalib",
                         "m_createSelectedContainer": True,
                         "m_outContainerName": "SelectedJets450",
                         "m_pT_min": 450000,
                         "m_eta_max": 1.7
                        })

c.setalg("JetHistsAlgo", {"m_debug": False,
                          "m_inContainerName": "SelectedJets450",
                          "m_detailStr": "kinematic clean energy layer trackPV",
                          "m_name": "SelectedJets450"
                        })

c.setalg("TreeAlgo", {"m_debug": False,
                      "m_name": "ClusterTest",
                      "m_jetContainerName": "AntiKt4EMTopoJetsCalib",
                      "m_jetDetailStr": "kinematic energy scales substructure truth trackPV layer",
                      "m_trigDetailStr": "basic menuKeys passTriggers",
                      "m_evtDetailStr": "pileup shapeEM shapeLC"
                      #"m_evtDetailStr": "pileup shapeEM shapeLC caloClusters"
                    })
